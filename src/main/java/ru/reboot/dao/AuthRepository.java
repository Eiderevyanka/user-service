package ru.reboot.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.reboot.dao.entity.UserEntity;

import java.util.List;
import java.util.Optional;

public interface AuthRepository extends JpaRepository<UserEntity, String> {

    Optional<UserEntity> findByLogin(String login);

    @Query("SELECT u FROM UserEntity u WHERE lower(u.roles) LIKE %:role%")
    List<UserEntity> findUsersByRoleIgnoreCase (@Param("role") String role);

}
