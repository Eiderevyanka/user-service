package ru.reboot.dto;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

public class User {
    private String userId;
    private String firstName;
    private String lastName;
    private String secondName;
    private String birthDate;
    private String login;
    private String password;
    private List<String> roles;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        String stringRoles;
        if (roles != null) {
            stringRoles = String.join(",", roles);
        } else {
            stringRoles = null;
        }
        return "User{" +
                "user_id='" + userId + '\'' +
                ", first_name='" + firstName + '\'' +
                ", last_name='" + lastName + '\'' +
                ", second_name='" + secondName + '\'' +
                ", birth_date=" + birthDate +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", roles='" + stringRoles + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(userId, user.userId) && Objects.equals(firstName, user.firstName) && Objects.equals(lastName, user.lastName) && Objects.equals(secondName, user.secondName) && Objects.equals(birthDate, user.birthDate) && Objects.equals(login, user.login) && Objects.equals(password, user.password) && Objects.equals(roles, user.roles);
    }

    public static class Builder {
        private User obj;

        public Builder() {
            obj = new User();
        }

        public Builder setUserID(String userId) {
            obj.userId = userId;
            return this;
        }

        public Builder setFirstName(String firstName) {
            obj.firstName = firstName;
            return this;
        }

        public Builder setLastName(String lastName) {
            obj.lastName = lastName;
            return this;
        }

        public Builder setSecondName(String secondName) {
            obj.secondName = secondName;
            return this;
        }

        public Builder setBirthDate(String birthDate) {
            obj.birthDate = birthDate;
            return this;
        }

        public Builder setLogin(String login) {
            obj.login = login;
            return this;
        }

        public Builder setPassword(String password) {
            obj.password = password;
            return this;
        }

        public Builder setRoles(List<String> roles) {
            obj.roles = roles;
            return this;
        }

        public User build() {
            return obj;
        }
    }
}
