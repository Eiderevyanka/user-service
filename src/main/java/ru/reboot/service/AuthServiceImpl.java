package ru.reboot.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.reboot.dao.AuthRepository;
import ru.reboot.dao.entity.UserEntity;
import ru.reboot.dto.User;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.error.ErrorCodes;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class AuthServiceImpl implements AuthService {

    private static final Logger logger = LogManager.getLogger(AuthServiceImpl.class);
    private AuthRepository authRepository;

    @Autowired
    public void setAuthRepository(AuthRepository authRepository) {
        this.authRepository = authRepository;
    }

    /**
     * Returns registered user from database with specified user_id
     * or throws BusinessLogicException
     *
     * @param userId - user id
     * @return User
     */
    @Override
    public User getUserByUserId(String userId) {

        logger.info("Method .getUserByUserId started userId={}", userId);
        try {
            if (Objects.isNull(userId) || userId.equals("")) {
                throw new BusinessLogicException("User id can't be null or empty ", ErrorCodes.ILLEGAL_ARGUMENT.toString());
            }
            User user = convertUserEntityToUser(this.authRepository.findById(userId)
                    .orElseThrow(() -> new BusinessLogicException("User not found ", ErrorCodes.USER_NOT_FOUND.toString())));
            logger.info("Method .getUserByUserId completed user={}", user);
            return user;
        } catch (Exception e) {
            logger.error("Failed to .getUserByUserId error={}", e.toString(), e);
            throw e;
        }
    }

    /**
     * Returns registered user from database with specified login
     * or throws BusinessLogicException
     *
     * @param login - user login
     * @return User
     */
    @Override
    public User getUserByLogin(String login) {

        logger.info("Method .getUserByLogin started login={}", login);
        try {
            if (Objects.isNull(login) || login.equals("")) {
                throw new BusinessLogicException("Login can't be null or empty ", ErrorCodes.ILLEGAL_ARGUMENT.toString());
            }
            User user = convertUserEntityToUser((this.authRepository.findByLogin(login)
                    .orElseThrow(() -> new BusinessLogicException("User not found ", ErrorCodes.USER_NOT_FOUND.toString()))));
            logger.info("Method .getUserByLogin completed user={}", user);
            return user;
        } catch (Exception e) {
            logger.error("Failed to .getUserByLogin error={}", e.toString(), e);
            throw e;
        }
    }

    /**
     * Method delete user by id that gets in params
     *
     * @param userId
     */
    @Override
    public void deleteUser(String userId) {
        logger.info("Method .deleteUser");
        if (Objects.isNull(userId) || userId.equals("")) {
            throw new BusinessLogicException("User id can't be null or empty", ErrorCodes.ILLEGAL_ARGUMENT.toString());
        }
        try {
            authRepository.deleteById(userId);
            logger.info("Method .deleteUser completed, delete userId={}", userId);
        } catch (BusinessLogicException ex) {
            logger.error("Failed to .getUserByLogin error={}", ex.toString(), ex);
            throw ex;
        } catch (Exception ex) {
            logger.error("Failed to .getUserByLogin error={}", ex.toString(), ex);
            throw new BusinessLogicException(ex.getMessage(), ErrorCodes.USER_NOT_FOUND.toString());
        }
    }

    /**
     * Service method of creating user
     *
     * @param user - user,that needs to be created in DB
     * @return Passed user if success creating or throw exception BusinessLogicException.class if USER_NOT_CONSISTENT or USER_ALREADY_EXISTS
     */
    @Override
    public User createUser(User user) {

        try {
            logger.info("Method .createUser");

            validateUser(user);

            if (this.authRepository.findById(user.getUserId()).isPresent()) {
                throw new BusinessLogicException("User already exists", ErrorCodes.DUPLICATE_USERID.toString());
            }
            if (this.authRepository.findByLogin(user.getLogin()).isPresent()) {
                throw new BusinessLogicException("User with this login exists", ErrorCodes.DUPLICATE_LOGIN.toString());
            }

            UserEntity userEntity = convertUserToUserEntity(user);
            this.authRepository.save(userEntity);
            logger.info("Method .createUser completed user={}", user);
            return user;

        } catch (BusinessLogicException e) {
            logger.error("Failed to .createUser error={}", e.toString(), e);
            throw e;
        } catch (Exception ex) {
            logger.error("Failed to .getUserByLogin error={}", ex.toString(), ex);
            throw new BusinessLogicException(ex.getMessage(), ErrorCodes.CANT_CREATE_NEW_USER.toString());
        }
    }

    /**
     * Service method of updating user
     *
     * @param user - user,that needs to be updated in DB
     * @return Passed user if success updating or throw exception BusinessLogicException.class if USER_NOT_CONSISTENT or USER_NOT_EXISTS
     */
    @Override
    public User updateUser(User user) {

        try {
            logger.info("Method .updateUser");

            validateUser(user);

            Optional<UserEntity> foundUserById = this.authRepository.findById(user.getUserId());
            Optional<UserEntity> foundUserByLogin = this.authRepository.findByLogin(user.getLogin());

            if (!foundUserById.isPresent()) {
                throw new BusinessLogicException("User doesn't exists", ErrorCodes.USER_NOT_FOUND.toString());
            } else {
                if (foundUserByLogin.isPresent() && !foundUserByLogin.get().getUserId().equals(user.getUserId())) {
                    throw new BusinessLogicException("User with this login exists",
                            ErrorCodes.DUPLICATE_LOGIN.toString());
                }
            }

            UserEntity userEntity = convertUserToUserEntity(user);
            this.authRepository.save(userEntity);
            logger.info("Method .updateUser completed user={}", user);
            return user;

        } catch (BusinessLogicException e) {
            logger.error("Failed to .updateUser error={}", e.toString(), e);
            throw e;
        } catch (Exception ex) {
            logger.error("Failed to .getUserByLogin error={}", ex.toString(), ex);
            throw new BusinessLogicException(ex.getMessage(), ErrorCodes.CANT_UPDATE_USER.toString());
        }
    }

    /**
     * Method gets all user from repository and return list of users
     *
     * @return List<User>
     */
    @Override
    public List<User> getAllUsers() {

        logger.info("Method .getAllUsers started");
        try {
            List<User> users = authRepository.findAll().stream()
                    .map(this::convertUserEntityToUser)
                    .collect(Collectors.toList());

            logger.info("Method .getAllUsers completed result={}", users);
            return users;
        } catch (Exception e) {
            logger.error("Failed to .getAllUsers error={}", e.toString(), e);
            throw e;
        }
    }

    /**
     * Method gets collection of roles in params and gets all users from repository and return user list only have a role contained in roles collection
     *
     * @param roles
     * @return List<User>
     */
    @Override
    public List<User> getAllUsersByRole(Collection<String> roles) {
        logger.info("Method .getAllUserByRole");
        List<User> listUsers = new ArrayList<>();
        List<UserEntity> tmpUsers;
        try {
            for (String role : roles) {
                tmpUsers = authRepository.findUsersByRoleIgnoreCase(role.toLowerCase(Locale.ROOT));
                for (UserEntity u : tmpUsers) {
                    User user = convertUserEntityToUser(u);
                    if (!listUsers.contains(user)) {
                        listUsers.add(user);
                    }
                }
            }
            logger.info("Method .getAllUserByRole completed, find users={}", listUsers);
            return listUsers;
        } catch (Exception ex) {
            logger.error("Failed to .getAllUsers error={}", ex.toString(), ex);
            throw ex;
        }
    }

    /**
     * The method converts an object from UserEntity to User
     *
     * @param userEntity
     * @return
     */
    public User convertUserEntityToUser(UserEntity userEntity) {

        return new User.Builder()
                .setUserID(userEntity.getUserId())
                .setFirstName(userEntity.getFirstName())
                .setLastName(userEntity.getLastName())
                .setSecondName(userEntity.getSecondName())
                .setBirthDate(userEntity.getBirthDate())
                .setLogin(userEntity.getLogin())
                .setPassword(userEntity.getPassword())
                .setRoles(Stream.of(userEntity.getRoles().split(","))
                        .collect(Collectors.toList()))
                .build();
    }

    /**
     * The method converts the object from User to UserEntity
     *
     * @param user
     * @return {@link UserEntity}
     */
    public UserEntity convertUserToUserEntity(User user) {

        return new UserEntity.Builder()
                .setUserID(user.getUserId())
                .setFirstName(user.getFirstName())
                .setLastName(user.getLastName())
                .setSecondName(user.getSecondName())
                .setBirthDate(user.getBirthDate())
                .setLogin(user.getLogin())
                .setPassword(user.getPassword())
                .setRoles(user.getRoles())
                .build();
    }

    private void validateUser(User user) {

        if (Objects.isNull(user)) {
            throw new BusinessLogicException("User can't be null",
                    ErrorCodes.ILLEGAL_ARGUMENT.toString());
        }

        String userId = user.getUserId();
        String firstName = user.getFirstName();
        String lastName = user.getLastName();
        String secondName = user.getSecondName();
        String birthDate = user.getBirthDate();
        String login = user.getLogin();
        String password = user.getPassword();
        List<String> roles = user.getRoles();

        if (Objects.isNull(userId) || userId.equals("") ||
                Objects.isNull(firstName) || firstName.equals("") ||
                Objects.isNull(lastName) || lastName.equals("") ||
                Objects.isNull(secondName) || secondName.equals("") ||
                Objects.isNull(birthDate) ||
                Objects.isNull(login) || login.equals("") ||
                Objects.isNull(password) || password.equals("") ||
                Objects.isNull(roles) || roles.isEmpty()) {
            throw new BusinessLogicException("User fields can't be null or empty",
                    ErrorCodes.ILLEGAL_ARGUMENT.toString());
        }
    }
}
