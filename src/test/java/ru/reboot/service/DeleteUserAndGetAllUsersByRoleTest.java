package ru.reboot.service;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import ru.reboot.dao.AuthRepository;
import ru.reboot.dao.entity.UserEntity;
import ru.reboot.dto.User;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.error.ErrorCodes;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;


public class DeleteUserAndGetAllUsersByRoleTest {

    @Test
    public void deleteUser_Positive_Test() {
        AuthRepository authRepository = Mockito.mock(AuthRepository.class);
        AuthServiceImpl authService = new AuthServiceImpl();

        authService.setAuthRepository(authRepository);

        authService.deleteUser("userId");

        Mockito.verify(authRepository).deleteById("userId");
    }

    @Test
    public void deleteUserWhenUserIdIsNull_Test() {
        AuthRepository authRepository = Mockito.mock(AuthRepository.class);
        AuthServiceImpl authService = new AuthServiceImpl();

        authService.setAuthRepository(authRepository);

        try {
            authService.deleteUser(null);
            Assert.fail();
        } catch (BusinessLogicException ex) {
            Assert.assertEquals(ex.getCode(), ErrorCodes.ILLEGAL_ARGUMENT.toString());
        }

        Mockito.verify(authRepository, Mockito.times(0)).deleteById(null);
    }

    @Test
    public void getAllUserByRole_Positive_Test() {
        AuthRepository authRepository = Mockito.mock(AuthRepository.class);
        AuthServiceImpl authService = new AuthServiceImpl();

        authService.setAuthRepository(authRepository);

        UserEntity firstUserEntity = new UserEntity();
        UserEntity secondUserEntity = new UserEntity();

        firstUserEntity.setUserId("x1");
        firstUserEntity.setRoles("user");
        secondUserEntity.setUserId("x2");
        secondUserEntity.setRoles("admin");

        Mockito.when(authRepository.findUsersByRoleIgnoreCase(Mockito.anyString())).thenReturn(Arrays.asList(firstUserEntity, secondUserEntity));

        List<User> users = authService.getAllUsersByRole(Arrays.asList("user", "admin"));

        Assert.assertEquals(users.size(), 2);

        Mockito.verify(authRepository, Mockito.times(2)).findUsersByRoleIgnoreCase(Mockito.anyString());
    }

    @Test
    public void getAllUserByRole_Negative_WhenUsersNotFound_Test() {
        AuthRepository authRepository = Mockito.mock(AuthRepository.class);
        AuthServiceImpl authService = new AuthServiceImpl();

        authService.setAuthRepository(authRepository);

        UserEntity firstUserEntity = new UserEntity();
        UserEntity secondUserEntity = new UserEntity();

        firstUserEntity.setUserId("x1");
        firstUserEntity.setRoles("user");
        secondUserEntity.setUserId("x2");
        secondUserEntity.setRoles("user");

        Mockito.when(authRepository.findUsersByRoleIgnoreCase(Mockito.anyString())).thenThrow(new NoSuchElementException("test"));

        try {
            List<User> users = authService.getAllUsersByRole(Arrays.asList("admin"));
            Assert.fail();
        } catch (NoSuchElementException ex) {
            Assert.assertEquals(ex.getMessage(), "test");
        }
        Mockito.verify(authRepository, Mockito.times(1)).findUsersByRoleIgnoreCase(Mockito.anyString());
    }

}
