package ru.reboot.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.reboot.dao.AuthRepository;
import ru.reboot.dao.entity.UserEntity;
import ru.reboot.dto.User;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.error.ErrorCodes;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Optional;

import static org.mockito.Mockito.*;

public class CreateUserTest {

    private AuthRepository mockRepository = mock(AuthRepository.class);
    private AuthServiceImpl authService = new AuthServiceImpl();
    private User user;
    private UserEntity mockEntity = mock(UserEntity.class);

    @Before
    public void init() {
        authService.setAuthRepository(mockRepository);

        user = new User();
        user.setUserId("x6");
        user.setFirstName("Mark");
        user.setLastName("Markov");
        user.setSecondName("Mark");
        user.setBirthDate(LocalDate.now().toString());
        user.setLogin("mark");
        user.setPassword("123");
        user.setRoles(Collections.singletonList("ADMIN"));
    }

    @Test
        public void createUserTest_wrongData() {
        user.setUserId("");

        try {
            authService.createUser(user);
            verifyZeroInteractions(mockRepository.findById(user.getUserId()));
            Assert.fail();
        } catch (BusinessLogicException e) {
            Assert.assertEquals(ErrorCodes.ILLEGAL_ARGUMENT.toString(), e.getCode());
        }
    }

    @Test
    public void createUserTest_userAlreadyExists() {
        when(mockRepository.findById(user.getUserId())).thenReturn(Optional.of(mockEntity));

        try {
            authService.createUser(user);
            verify(mockRepository.findById(user.getUserId()));
            verifyZeroInteractions(mockRepository.save(mockEntity));
            Assert.fail();
        } catch (BusinessLogicException e) {
            Assert.assertEquals(ErrorCodes.DUPLICATE_USERID.toString(), e.getCode());
        }
    }

    @Test
    public void createUserTest_loginAlreadyExists() {
        when(mockRepository.findByLogin(user.getLogin())).thenReturn(Optional.of(mockEntity));

        try {
            authService.createUser(user);
            verify(mockRepository.findByLogin(user.getLogin()));
            verifyZeroInteractions(mockRepository.save(mockEntity));
            Assert.fail();
        } catch (BusinessLogicException e) {
            Assert.assertEquals(ErrorCodes.DUPLICATE_LOGIN.toString(), e.getCode());
        }
    }

    @Test
    public void createUserTest_positive() {
        when(mockRepository.findById(user.getUserId())).thenReturn(Optional.empty());

        User createdUser = authService.createUser(user);

        Assert.assertEquals(user.getUserId(), createdUser.getUserId());
        Assert.assertEquals(user.getFirstName(), createdUser.getFirstName());
        Assert.assertEquals(user.getLogin(), createdUser.getLogin());
        Assert.assertEquals(user.getPassword(), createdUser.getPassword());
    }
}
