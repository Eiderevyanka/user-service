package ru.reboot.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.reboot.dao.AuthRepository;
import ru.reboot.dao.entity.UserEntity;
import ru.reboot.dto.User;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.error.ErrorCodes;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Optional;

import static org.mockito.Mockito.*;

public class UpdateUserTest {

    private AuthRepository mockRepository = mock(AuthRepository.class);
    private AuthServiceImpl authService = new AuthServiceImpl();
    private User user;
    private UserEntity mockEntity = mock(UserEntity.class);

    @Before
    public void init() {
        authService.setAuthRepository(mockRepository);

        user = new User();
        user.setUserId("x6");
        user.setFirstName("Mark");
        user.setLastName("Markov");
        user.setSecondName("Mark");
        user.setBirthDate(LocalDate.now().toString());
        user.setLogin("mark");
        user.setPassword("123");
        user.setRoles(Collections.singletonList("ADMIN"));
    }

    @Test
    public void updateUserTest_wrongData() {
        user.setUserId("");

        try {
            authService.updateUser(user);
            verifyZeroInteractions(mockRepository.findById(user.getUserId()));
            Assert.fail();
        } catch (BusinessLogicException e) {
            Assert.assertEquals(ErrorCodes.ILLEGAL_ARGUMENT.toString(), e.getCode());
        }
    }

    @Test
    public void updateUserTest_userDoesntExist() {
        when(mockRepository.findById(user.getUserId())).thenReturn(Optional.empty());

        try {
            authService.updateUser(user);
            verify(mockRepository.findById(user.getUserId()));
            verifyZeroInteractions(mockRepository.save(mockEntity));
            Assert.fail();
        } catch (BusinessLogicException e) {
            Assert.assertEquals(ErrorCodes.USER_NOT_FOUND.toString(), e.getCode());
        }
    }

    @Test
    public void updateUserTest_loginAlreadyExists() {

        UserEntity userEntity = new UserEntity.Builder()
                .setUserID("x8")
                .setLogin("mark")
                .build();

        when(mockRepository.findById(user.getUserId())).thenReturn(Optional.of(mockEntity));
        when(mockRepository.findByLogin(user.getLogin())).thenReturn(Optional.of(userEntity));

        try {
            authService.updateUser(user);
            verify(mockRepository.findById(user.getUserId()));
            verify(mockRepository.findByLogin(user.getLogin()));
            verifyZeroInteractions(mockRepository.save(mockEntity));
            Assert.fail();
        } catch (BusinessLogicException e) {
            Assert.assertEquals(ErrorCodes.DUPLICATE_LOGIN.toString(), e.getCode());
        }
    }

    @Test
    public void updateUserTest_positive() {
        when(mockRepository.findById(user.getUserId())).thenReturn(Optional.of(mockEntity));

        User createdUser = authService.updateUser(user);

        Assert.assertEquals(user.getUserId(), createdUser.getUserId());
        Assert.assertEquals(user.getFirstName(), createdUser.getFirstName());
        Assert.assertEquals(user.getLogin(), createdUser.getLogin());
        Assert.assertEquals(user.getPassword(), createdUser.getPassword());
    }
}
