package ru.reboot.service;

import junit.framework.TestCase;
import org.junit.Assert;
import org.mockito.Mockito;
import ru.reboot.dao.AuthRepository;
import ru.reboot.dao.entity.UserEntity;
import ru.reboot.dto.User;
import ru.reboot.error.BusinessLogicException;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class GetUserByUserId_GetUserByLogin_Test extends TestCase {

    UserEntity userEntity = new UserEntity.Builder().setUserID("1")
            .setFirstName("Maxim").setLastName("Ivanov").setSecondName("Alexeevitch")
            .setLogin("max").setPassword("passOne")
            .setBirthDate(LocalDate.of(1985, 05, 26).toString())
            .setRoles(Collections.singletonList("ADMIN")).build();



    UserEntity userEntityDoubleRoles = new UserEntity.Builder().setUserID("3")
            .setFirstName("Egor").setLastName("Sergeev").setSecondName("Petrovich")
            .setLogin("login").setPassword("passThree")
            .setBirthDate(LocalDate.of(1981, 10, 05).toString())
            .setRoles(Arrays.asList("ADMIN","USER")).build();


    public void testGetUserByUserIdPositive() {

        AuthRepository authRepository = Mockito.mock(AuthRepository.class);
        AuthServiceImpl authService = new AuthServiceImpl();

        authService.setAuthRepository(authRepository);

        Mockito.when(authRepository.findById("1")).thenReturn(Optional.of(userEntity));

        User user = authService.getUserByUserId(userEntity.getUserId());
        Assert.assertEquals(userEntity.getUserId(), user.getUserId());
        Assert.assertEquals(userEntity.getLogin(), user.getLogin());
        Assert.assertEquals(Stream.of(userEntity.getRoles().split(","))
                .collect(Collectors.toList()), user.getRoles());

        Mockito.verify(authRepository, Mockito.times(1)).findById(Mockito.anyString());
    }

    public void testGetUserByUserIdWhenIllegalArgument() {

        AuthRepository authRepository = Mockito.mock(AuthRepository.class);
        AuthServiceImpl authService = new AuthServiceImpl();

        authService.setAuthRepository(authRepository);

        Mockito.when(authRepository.findById("")).thenThrow(BusinessLogicException.class);

        try {
            authService.getUserByUserId("");
            Assert.fail();
        } catch (BusinessLogicException ex){
            Assert.assertEquals(ex.getCode(), "ILLEGAL_ARGUMENT");
        }
    }

    public void testGetUserByUserIdWhenUserNotFound() {

        AuthRepository authRepository = Mockito.mock(AuthRepository.class);
        AuthServiceImpl authService = new AuthServiceImpl();

        authService.setAuthRepository(authRepository);

        Mockito.when(authRepository.findById(Mockito.anyString())).thenReturn(Optional.ofNullable(null));

        try {
            authService.getUserByUserId("5");
            Assert.fail();
        } catch (BusinessLogicException ex){
                Assert.assertEquals(ex.getCode(), "USER_NOT_FOUND");
            }
        }


        public void testGetUserByLogin_Positive() {

        AuthRepository authRepository = Mockito.mock(AuthRepository.class);
        AuthServiceImpl authService = new AuthServiceImpl();

        authService.setAuthRepository(authRepository);

        Mockito.when(authRepository.findByLogin("login")).thenReturn(Optional.of(userEntityDoubleRoles));

        User user = authService.getUserByLogin(userEntityDoubleRoles.getLogin());
        Assert.assertEquals(userEntityDoubleRoles.getUserId(), user.getUserId());
        Assert.assertEquals(userEntityDoubleRoles.getLogin(), user.getLogin());
        Assert.assertEquals(Stream.of(userEntityDoubleRoles.getRoles().split(","))
                .collect(Collectors.toList()), user.getRoles());

    }

    public void testGetUserByLoginWhenIllegalArgument() {

        AuthRepository authRepository = Mockito.mock(AuthRepository.class);
        AuthServiceImpl authService = new AuthServiceImpl();

        authService.setAuthRepository(authRepository);

        Mockito.when(authRepository.findByLogin(null)).thenThrow(BusinessLogicException.class);

        try {
            authService.getUserByLogin(null);
            Assert.fail();
        } catch (BusinessLogicException ex){
            Assert.assertEquals(ex.getCode(), "ILLEGAL_ARGUMENT");
        }
    }

    public void testGetUserByLoginWhenUserNotFound() {

        AuthRepository authRepository = Mockito.mock(AuthRepository.class);
        AuthServiceImpl authService = new AuthServiceImpl();

        authService.setAuthRepository(authRepository);

        Mockito.when(authRepository.findByLogin(Mockito.anyString())).thenReturn(Optional.ofNullable(null));

        try {
            authService.getUserByLogin("wrongLogin");
            Assert.fail();
        } catch (BusinessLogicException ex){
            Assert.assertEquals(ex.getCode(), "USER_NOT_FOUND");
        }
    }

}