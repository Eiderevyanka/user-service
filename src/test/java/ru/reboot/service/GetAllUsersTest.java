package ru.reboot.service;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import ru.reboot.dao.AuthRepository;
import ru.reboot.dao.entity.UserEntity;
import ru.reboot.dto.User;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class GetAllUsersTest {

    @Test
    public void getAllUsersTest() {

        AuthRepository authRepository = Mockito.mock(AuthRepository.class);
        AuthServiceImpl authService = new AuthServiceImpl();
        authService.setAuthRepository(authRepository);

        List<UserEntity> userEntities = new ArrayList<>();
        Mockito.when(authRepository.findAll()).thenReturn(userEntities);

        Assert.assertEquals(authRepository.findAll(), Collections.emptyList());

        for (int i = 1; i < 6; i++) {
            userEntities.add(new UserEntity.Builder()
                    .setUserID("id" + i)
                    .setFirstName("firstName" + i)
                    .setLastName("lastName" + i)
                    .setSecondName("secondName" + i)
                    .setLogin("login" + i)
                    .setPassword("password" + i)
                    .setBirthDate(LocalDate.now().toString())
                    .setRoles(Arrays.asList("ADMIN", "USER"))
                    .build());
        }

        List<User> users = userEntities.stream()
                .map(authService::convertUserEntityToUser)
                .collect(Collectors.toList());

        List<User> allUsers = authService.getAllUsers();

        for (int i = 0; i < users.size(); i++) {
            Assert.assertEquals(allUsers.get(i).getUserId(), users.get(i).getUserId());
            Assert.assertEquals(allUsers.get(i).getFirstName(), users.get(i).getFirstName());
            Assert.assertEquals(allUsers.get(i).getLastName(), users.get(i).getLastName());
            Assert.assertEquals(allUsers.get(i).getSecondName(), users.get(i).getSecondName());
            Assert.assertEquals(allUsers.get(i).getSecondName(), users.get(i).getSecondName());
            Assert.assertEquals(allUsers.get(i).getSecondName(), users.get(i).getSecondName());
            Assert.assertEquals(allUsers.get(i).getLogin(), users.get(i).getLogin());
            Assert.assertEquals(allUsers.get(i).getPassword(), users.get(i).getPassword());
            Assert.assertEquals(allUsers.get(i).getBirthDate(), users.get(i).getBirthDate());
            Assert.assertEquals(allUsers.get(i).getRoles(), users.get(i).getRoles());
        }

        Mockito.verify(authRepository, Mockito.times(2)).findAll();
    }
}